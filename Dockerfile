FROM ubuntu:16.04

RUN apt-get update
ENV GOOGLE_APPLICATION_CREDENTIALS=./gcloud.json
RUN mkdir /pd_sentiment
COPY /pd_sentiment/ /pd_sentiment/
RUN apt-get update && \
        apt-get install -y software-properties-common && \
        add-apt-repository ppa:fkrull/deadsnakes && \
        apt-get update -y  && \
        apt-get install -y build-essential locales python3.6 python3.6-dev python3-pip python3-setuptools && \
        apt-get install -y git  libmysqlclient-dev && \
        python3.6 -m pip install pip==20.0.1 && \
        python3.6 -m pip install wheel && \
        python3.6 -m pip install mysqlclient && \
        python3.6 -m pip install pymysql
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
RUN pip3 install -r /pd_sentiment/requirements.txt
RUN python3.6 -m pip install -U pip setuptools 
RUN python3.6 -m pip install mysqlclient && \
    python3.6 -m pip install pymysql
RUN cd /pd_sentiment/torchMoji/ && python3.6 setup.py install
RUN python3.6 -m nltk.downloader punkt
RUN python3.6 -m nltk.downloader stopwords
ENV HOST_NAME=172.17.0.1
WORKDIR /pd_sentiment
ENTRYPOINT [ "python3.6" ]
CMD ["sanic_sentiment_api.py"]

