import sys
from sanic import Sanic
from sanic.response import  json
from sanic import response
from sanic_sentry import SanicSentry
from sanic_cors import CORS, cross_origin
import requests
from torchMoji import test_sentiment_texts_v3,test_sentiment_texts
import urllib.parse
import traceback
import translate
import settings
import security
import logging
import json
import math
import ast 
from goose3 import Goose
import sanic_limiter_func
from imdb_sentiment import get_label_imdb_sentiment
import random
import datetime
import time
import requests
from settings import error_500

app = Sanic(__name__)


#app.config['SENTRY_DSN'] = "https://b4b3b106a12346fd8b9cf719b8a04150:3e34063899cc4b80899a5907a955b6be@sentry.io/1199764"

logging.basicConfig(level=logging.ERROR, filename='error_data.log', format='%(asctime)s %(levelname)s:%(message)s')


#plugin = SanicSentry()
#plugin.init_app(app)

CORS(app)

g=Goose()

def sentiment_social_call( text ):
	if len(text)==2:
		if text[1] == "imdb":
			try:
				resp = get_label_imdb_sentiment(text)
			except Exception as e:
				logging.error("{} - sentence entered by user ".format(text))
			output={}
			if resp :
				if type(resp) == str:
					resp = json.loads(resp)
				result = {"sentiment":resp}
				return result
		else:
			try:
				result		= test_sentiment_texts( text[0] )
				return result
			except Exception as e:
				logging.error("{} - sentence entered by user ".format(text[0]))
	else:
		try:
			result		= test_sentiment_texts( text[0] )
			return result
		except Exception as e:
			logging.error("{} - sentence entered by user ".format(text[0]))


def sentiment_social_call_v3( text ):
	try:
		resp		= test_sentiment_texts_v3( text[0] )
		return resp
	except Exception as e:
		logging.error("{} - sentence entered by user ".format(text[0]))


def sentiment_multilingual( text ):
	data = requests.post("http://127.0.0.1:7845/new_sentiment",data={"text":text})
	print("this is data",data)
	return data.json() 

@app.route("/v4/sentiment",methods= ['POST'])
async def sentiment(request):
	# try:
		
	# 	UA = request.headers.get("User-Agent" )
	# except:
	# 	UA = "NA"
	start = time.time()	
	api_type     = "sentiment"
	headers      = request.headers
	# print("-->>headers",headers)
	real_ip      = headers.get( "X-Real-Ip" )
	source       = headers.get( "source", None )
	user_agent   = headers.get("User-Agent",None  )
	api_key      = request.form.get( "api_key", None )
	model		 = request.form.get("model","basic")
	lang_code    = request.form.get( "lang_code", "en" )
	text         = request.form.get( "text", None)
	if text :
		pass
	else:
		url = request.form.get("url", None)
		if url:
			text=g.extract(url=url)
			text=text.cleaned_text
	# print(text,api_key,lang_code,lang_code)
	if api_key == None or api_key == "" or text == None or type( text ) == None or  text == "":
		output = {"code":400,"Error":"Parameter `API Key`  or `TEXT` is either not specified or set to null."}
		#security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		# print("this is time taken ",time.time()-start)
		return response.json(output)

	if len(text.strip())==0:
		output = {"code":400,"Error":"Parameter `TEXT` can not be blank or null."}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		# print("this is time taken ",time.time()-start)
		return response.json(output)


	status = security.check_api_key_blocked(api_key)
	if status == True:
		output = {"code":401,"Error":"Your API KEY is blocked"}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		# print("this is time taken ",time.time()-start)
		return response.json(output)


	if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
		# print("google sheet or excel")
		resp = {"code":1}
	else:
		# resp = {"code":1}
		status = security.check_api_key_rate_limit_restriction(api_key)
		if status == True:
			resp = {"code":1}
		else:
			resp=sanic_limiter_func.check_paid_user(api_key,lang_code)
	# print(datetime.datetime.now(),resp,api_key)
	
	if resp['code'] == 403:
		# print("this is time taken ",time.time()-start)
		return response.json({"code":403,"Error":"You are currently subscribed to the "+resp['package_name']+" plan. Please upgrade your account from your dashboard to use ParallelDots APIs in languages other than English, Dutch, German, French, Italian and Spanish."})

	if resp['code'] == 429:
		# print("this is time taken ",time.time()-start)
		return response.json({"code":429,"Error":"Oops! You have exceeded the rate limit. Please upgrade your plan to get higher rate limits - https://user.apis.paralleldots.com/user_dashboard."})
	
	if resp['code'] == 400:
		# print("this is time taken ",time.time()-start)
		return response.json({"code":400,"Error":"Invalid API Key"})

	if api_key =="FpZhVrSoSoxTpCXspZ3QOnJwFqBXbRXjEfzSDVLsOAg":
		print("special check for pliik.io")
		print(real_ip,source,user_agent)
		if security.check_daily_quota(api_key):
			return response.json({ "Error": "Daily Limit Exceeded. Please upgrade your account from your user dashboard at https://user.apis.paralleldots.com/user_dashboard", "code": 403 })

	weights     = math.ceil( len( text ) / settings.size )
	

	if lang_code == "en":
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, sentiment_social_call, text )
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )	
		else:		
			output = security.validate_and_execute( api_key, api_type, weights, sentiment_social_call, text,model )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		print("this is time taken ",time.time()-start)
		return response.json(output)

	elif lang_code not in settings.iso_language_codes:
		output = settings.error_lang_codes
		security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		print("time_taken",time.time()-start)
		return response.json(output)

	elif lang_code in ["nl","de","fr","it","es"]:
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, sentiment_multilingual, text )
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )	
		else:		
			output = security.validate_and_execute( api_key, api_type, weights, sentiment_multilingual, text,model )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )				
		
		return response.json(output)

	else:
		text   = translate.azure_translate( text, lang_code )
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, sentiment_social_call, text )
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )	
		else:		
			output = security.validate_and_execute( api_key, api_type, weights, sentiment_social_call, text,model )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )				
		print("this is time taken ",time.time()-start)
		return response.json(output)



@app.route("/v4/sentiment_batch",methods= ['POST'])
async def sentiment_batch(request):
	api_type    = "sentiment"
	headers      = request.headers
	real_ip      = headers.get( "X-Real-Ip" )
	source       = headers.get( "host", None )
	user_agent   = headers.get("User-Agent",None  )
	api_key      = request.form.get( "api_key", None )
	model		 = request.form.get("model","basic")
	lang_code    = request.form.get( "lang_code", "en" )
	data         = request.form.get( "text", None)

	if api_key == None or api_key == "" or data == None or type( data ) == None or data == "":
		output = {"code":400,"Error":"Parameter `API Key`  or `TEXT` is either not specified or set to null."}
		#security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= data )
		return response.json(output)


	resp = sanic_limiter_func.check_paid_user(api_key,lang_code)
	if resp['code'] == 400:
		return response.json({"code":400,"Error":"Invalid API Key"})

	status = security.check_api_key_blocked(api_key)
	if status == True:
		output = {"code":401,"Error":"Your API KEY is blocked"}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= data )
		return response.json(output)

	try:
		data    = ast.literal_eval(data)
	except:
		output = settings.error_invalid_input_format
		return response.json(output)

	if type(data) is type(list()) or type(data) is type(tuple()):
		pass
	else:
		output = settings.error_invalid_input_format
		return response.json(output)

	return_array = []
	for text in data:
		weights = math.ceil( len( text ) / settings.size )

		if lang_code not in settings.iso_language_codes:
			output = settings.error_lang_codes
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
			return response.json(output)



		status = security.check_api_key_rate_limit_restriction(api_key)
		if status == True:
			resp = {"code":1}
		else:
			# resp = {"code":1}
			resp=sanic_limiter_func.check_paid_user(api_key,lang_code)

		if resp['code'] == 403:
			return response.json({"code":403,"Error":"You are currently subscribed to the "+resp['package_name']+" plan. Please upgrade your account from your dashboard to use ParallelDots APIs in languages other than English, Dutch, German, French, Italian and Spanish."})

		if resp['code'] == 429:
			return_array.append({"code":429,"Error":"Oops! You have exceeded the rate limit. Please upgrade your plan to get higher rate limits - https://user.apis.paralleldots.com/user_dashboard."})
		
		else:
		
			
			try:
				if lang_code == "en":
					output  = security.validate_and_execute( api_key, api_type, weights, sentiment_social_call, text, model )
					security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text )
				elif lang_code in ["nl","de","fr","it","es"]:		
					output = security.validate_and_execute( api_key, api_type, weights, sentiment_multilingual, text,model )
					security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )				
				else:
					text   = translate.azure_translate( text, lang_code )
					output = security.validate_and_execute( api_key, api_type, weights, sentiment_social_call, text, model )
					security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
			except:
				output  = None
				logging.error( "Error <500>: %s"%traceback.format_exc() )
				
			if output is not None:
				if 'Error' in output:
					return response.json(output)
				else:
					return_array.append( output['sentiment'] )
			else:
				return_array.append({})

	output = { "sentiment": return_array }
	return response.json(output)

@app.route("/v3/sentiment",methods= ['POST'])
async def sentiment(request):
	# try:
		
	# 	UA = request.headers.get("User-Agent" )
	# except:
	# 	UA = "NA"
			
	api_type     = "sentiment"
	headers      = request.headers
	# print(headers)
	real_ip      = headers.get( "X-Real-Ip" )
	source       = headers.get( "source", None )
	user_agent   = headers.get("User-Agent",None  )
	api_key      = request.form.get( "api_key", None )
	if api_key == None:
		print("5fg")
		api_key = request.args.get("api_key")
	lang_code    = request.form.get( "lang_code", "en" )
	text         = request.form.get( "text", None)
	if text == None:
		text = request.args.get("text")
	if text :
		pass
	else:
		url = request.form.get("url", None)
		if url:
			text=g.extract(url=url)
			text=text.cleaned_text
	print(text,api_key,lang_code)
	if api_key == None or api_key == "" or text == None or type( text ) == None or  text == "":
		output = {"code":400,"Error":"Parameter `API Key`  or `TEXT` is either not specified or set to null."}
		#security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)

	status = security.check_api_key_blocked(api_key)
	if status == True:
		output = {"code":401,"Error":"Your API KEY is blocked"}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)

	if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
		resp = {"code": 1}
	else:
		status = security.check_api_key_rate_limit_restriction(api_key)
		if status == True:
			resp = {"code":1}
		else:
			resp=sanic_limiter_func.check_paid_user(api_key,lang_code)

	if resp['code'] == 403:
		return response.json({"code":403,"Error":"You are currently subscribed to the "+resp['package_name']+" plan. Please upgrade your account from your dashboard to use ParallelDots APIs in languages other than English."})

	if resp['code'] == 429:
		return response.json({"code":429,"Error":"Oops! You have exceeded the rate limit. Please upgrade your plan to get higher rate limits - https://user.apis.paralleldots.com/user_dashboard."})
	
	if resp['code'] == 400:
		return response.json({"code":400,"Error":"Invalid API Key"})

	weights     = math.ceil( len( text ) / settings.size )
	

	if lang_code == "en":
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, sentiment_social_call_v3, text )
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )	
		else:		
			output = security.validate_and_execute( api_key, api_type, weights, sentiment_social_call_v3, text )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		output['code']=200
		return response.json(output)

	elif lang_code not in settings.iso_language_codes:
		output = settings.error_lang_codes
		security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		return response.json(output)

	else:
		text   = translate.azure_translate( text, lang_code )
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, sentiment_social_call_v3, text )
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )	
		else:		
			output = security.validate_and_execute( api_key, api_type, weights, sentiment_social_call_v3, text )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )				
		
		output['code']=200
		return response.json(output)



@app.route("/v3/sentiment_batch",methods= ['POST'])
async def sentiment_batch(request):
	api_type    = "sentiment"
	headers      = request.headers
	real_ip      = headers.get( "X-Real-Ip" )
	source       = headers.get( "source", None )
	user_agent   = headers.get("User-Agent",None  )
	api_key      = request.form.get( "api_key", None )
	if api_key == None:
		api_key = request.args.get("api_key")
	lang_code    = request.form.get( "lang_code", "en" )
	data         = request.form.get( "text", None)
	if data == None:
		data = request.args.get("text")

	if api_key == None or api_key == "" or data == None or type( data ) == None or data == "":
		output = {"code":400,"Error":"Parameter `API Key`  or `TEXT` is either not specified or set to null."}
		#security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= data )
		return response.json(output)
	# if data == None or type( data ) == None or data == "":
	# 	output = settings.error_param_text
	# 	security.register( "basic", real_ip, headers, api_type, api_key, None, output, data= data )
	# 	return response.json(output)

	resp = sanic_limiter_func.check_paid_user(api_key,lang_code)
	if resp['code'] == 400:
		return response.json({"code":400,"Error":"Invalid API Key"})

	status = security.check_api_key_blocked(api_key)
	if status == True:
		output = {"code":401,"Error":"Your API KEY is blocked"}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= data )
		return response.json(output)	

	try:
		data    = ast.literal_eval(data)
	except:
		output = settings.error_invalid_input_format
		return response.json(output)

	if type(data) is type(list()) or type(data) is type(tuple()):
		pass
	else:
		output = settings.error_invalid_input_format
		return response.json(output)

	return_array = []
	for text in data:
		weights = math.ceil( len( text ) / settings.size )
		if lang_code not in settings.iso_language_codes:
			output = settings.error_lang_codes
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
			return response.json(output)
			
		status = security.check_api_key_rate_limit_restriction(api_key)
		if status == True:
			resp = {"code":1}
		else:
			resp=sanic_limiter_func.check_paid_user(api_key,lang_code)
		# resp=sanic_limiter_func.check_paid_user(api_key,lang_code)
		if resp['code'] == 403:
			return response.json({"code":403,"Error":"You are currently subscribed to the "+resp['package_name']+" plan. Please upgrade your account from your dashboard to use ParallelDots APIs in languages other than English."})

		if resp['code'] == 429:
			return_array.append({"code":429,"Error":"Oops! You have exceeded the rate limit. Please upgrade your plan to get higher rate limits - https://user.apis.paralleldots.com/user_dashboard."})

		if resp['code'] == 400:
			return response.json({"code":400,"Error":"Invalid API Key"})	
		
		try:
			if lang_code == "en":
				output  = security.validate_and_execute( api_key, api_type, weights, sentiment_social_call_v3, text )
				security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text )
			else:
				text   = translate.azure_translate( text, lang_code )
				output = security.validate_and_execute( api_key, api_type, weights, sentiment_social_call_v3, text )
				security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		except:
			output  = None
			logging.error( "Error <500>: %s"%traceback.format_exc() )
			
		if output is not None:
			if 'Error' in output:
				return response.json(output)
			else:
				print(output)
				return_array.append( output['probabilities'] )
		else:
			return_array.append({})

	output = { "sentiment": return_array, "code":200 }
	return response.json(output)



if __name__ == "__main__":
        app.run(host="0.0.0.0",port=9203)
