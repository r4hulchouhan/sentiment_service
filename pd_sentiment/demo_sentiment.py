from sanic import Sanic
from sanic.response import  json
from sanic import response
from sanic_cors import CORS, cross_origin
import security
import settings
import logging
import json
import pymongo
import datetime
import torchMoji
import translate
app = Sanic()
CORS(app)


# path = "%svisual_api_images/"%settings.demo_path

MONGO_DB_SERVER_URL       = "mongodev.paralleldots.com"
DB_NAME                   = "api_server"

COLLECTION_HITS           = "demo"
conn      				  = pymongo.MongoClient(MONGO_DB_SERVER_URL)[ DB_NAME ][  COLLECTION_HITS  ]


def ip_filter(ip):
	if ip != '146.148.102.154' and ip != '35.226.67.193':
		code=204
		return code
	else:
		code=200
		return code


def register_hit( real_ip, headers, api_type, output, inputs ):
	conn.insert( { "timestamp": datetime.datetime.now(), "api_type": api_type, "real_ip": real_ip, "headers": headers, "output": output, "input": inputs } )


def referer_endpoint(referer):


	#referer = self.request.headers.get( "Referer" )
	logging.info( "Referer: %s"%referer )

	if referer == None or "paralleldots.com" not in referer :
		logging.error( "Invalid Referer." )
		raise Exception( "Invalid Referer." )
		return
def sentiment_social_call( text ):
	resp		= torchMoji.test_sentiment_texts_v3( text[0])
	resp['code']=200
	return resp

@app.route("/demo/sentiment",methods= ['POST'])
async def sentiment_demo( request ):
	api_type = "sentiment"
	headers     = request.headers
	real_ip     = headers.get( "X-Real-Ip" )
	referer     = headers.get( "Referer" )
	
	#output=ip_filter(real_ip)
	# if real_ip != "146.148.102.154" and real_ip != "35.226.67.193":
	# 	return response.json({"code":204,"output":"Access denied"})
	text   = request.form.get( "text", None )
	lang_code = request.form.get( "lang_code" , "en") 
	if lang_code == "en":
		output = security.keyless_validate_and_execute( sentiment_social_call, text )
		register_hit(real_ip,headers,api_type,output,text)
	else:
		text   = translate.aws_translate( text, lang_code )
		output = security.keyless_validate_and_execute( sentiment_social_call, text)
		# register_hit(real_ip,headers,api_type,output,text)
	#response.write( output )
	return response.json(output)

if __name__=="__main__":
	app.run(host="0.0.0.0",port=9002)
