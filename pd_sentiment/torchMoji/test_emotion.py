import torch
import numpy as np
import os
import json
from .torchmoji.sentence_tokenizer import SentenceTokenizer
from .torchmoji.global_variables import (
    VOCAB_PATH
)

#print (VOCAB_PATH)
import sys
from google.cloud import storage
import configparser
config = configparser.ConfigParser()

config.read('path.ini')
library_path=config['LIBRARY']['PATH']
model_path=config['MODEL']['PATH']
sys.path.append(library_path+"/torchmoji")

# sentiment
if os.path.isfile(model_path+"13march.sentiment")==True:
	model_sentiment = torch.load(model_path+'13march.sentiment')
else:
	client = storage.Client()
	bucket = client.get_bucket('apis_models')
	blob = bucket.blob('sentiment/13march.sentiment')
	blob.download_to_filename(model_path+'13march.sentiment')
	model_sentiment = torch.load(model_path+'13march.sentiment')



with open(VOCAB_PATH, 'r') as f:
	vocabulary = json.load(f)

max_len = 30
st = SentenceTokenizer(vocabulary, max_len)

def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0) # only difference

# def test_emotion_texts(TEST_SENTENCES):
# 	TEST_SENTENCES = [TEST_SENTENCES]
# 	tokenized, _, _ = st.tokenize_sentences(TEST_SENTENCES)
# 	pred = model_emotion(tokenized)
# 	pred = softmax(pred[0])
# 	# print(pred)
# 	output = {}
# 	out = {}
# 	output["happy"] = round(pred[0].astype(float),3)
# 	output["sad"] = round(pred[1].astype(float),3)
# 	output["excited"] =round(pred[2].astype(float),3)
# 	output["angry"] = round(pred[3].astype(float),3)
# 	output["indifferent"] = round(pred[4].astype(float),3)
# 	final_output = {}
# 	final_output["emotion"] = max(output, key=output.get)
# 	final_output["probabilities"] = output
# 	# out["emotion"]= final_output
# 	#print(final_output)
# 	return final_output

def test_sentiment_texts(TEST_SENTENCES):
	TEST_SENTENCES = [TEST_SENTENCES]
	#max_len = 30
	#st = SentenceTokenizer(vocabulary, max_len)
	tokenized, _, _ = st.tokenize_sentences(TEST_SENTENCES)
	pred = model_sentiment(tokenized)
	pred = softmax(pred[0])
	#print(pred)
	output = {}
	#out = {}
	output["negative"] = round(pred[0].astype(float),3)
	output["neutral"] = round(pred[1].astype(float),3)
	output["positive"] = round(pred[2].astype(float),3)
	#final_output = {}
	#final_output["sentiment"] = max(output, key=output.get)
	#final_output["probabilities"] = output

	# # out["sentiment"] = final_output

	# print(final_output)
	return {"sentiment":output}
	# return {"sentiment":[round(pred[2].astype(float),3),round(pred[1].astype(float),3),round(pred[0].astype(float),3)]}
	# TEST_SENTENCES = [TEST_SENTENCES]
	# #print TEST_SENTENCES[0]
	# max_len = 30
	# st = SentenceTokenizer(vocabulary, max_len)
	# tokenized, _, _ = st.tokenize_sentences(TEST_SENTENCES)
	# pred = model_sentiment(tokenized)
	# pred = softmax(pred[0])
	# #print(pred)
	# output = {}
	# out = {}
	# output["negative"] = round(pred[0].astype(float),3)
	# output["neutral"] = round(pred[1].astype(float),3)
	# output["positive"] = round(pred[2].astype(float),3)
	# final_output = {}
	# final_output["sentiment"] = max(output, key=output.get)
	# #print final_output["sentiment"]
	# #print "test sentence is",TEST_SENTENCES
	# for i in X:
	# 	#if "Budweiser" in i[0]:
	# 		#print "i0 is",i[0], i[1] 
	# 	if TEST_SENTENCES[0] == i[0].decode('ascii','ignore'):
	# 		#print "it's a match"
	# 		#print i[1]
	# 		if final_output != i[1]:
	# 			temp = output[i[1]]
	# 			output[i[1]] = output[final_output["sentiment"]]
	# 			output[final_output["sentiment"]] = temp
	# 			final_output["sentiment"] = i[1]

	# final_output["probabilities"] = output

	# out["sentiment"] = final_output

	# #print(final_output)
	# return (final_output)

def test_sentiment_texts_v3(TEST_SENTENCES):
	TEST_SENTENCES = [TEST_SENTENCES]
	#max_len = 30
	#st = SentenceTokenizer(vocabulary, max_len)
	tokenized, _, _ = st.tokenize_sentences(TEST_SENTENCES)
	pred = model_sentiment(tokenized)
	pred = softmax(pred[0])
	#print(pred)
	output = {}
	out = {}
	output["negative"] = round(pred[0].astype(float),3)
	output["neutral"] = round(pred[1].astype(float),3)
	output["positive"] = round(pred[2].astype(float),3)
	final_output = {}
	final_output["sentiment"] = max(output, key=output.get)
	final_output["probabilities"] = output

	

	# print(final_output)
	return final_output

def test_intent_texts(TEST_SENTENCES):
	TEST_SENTENCES = [TEST_SENTENCES]
	#max_len = 30
	#st = SentenceTokenizer(vocabulary, max_len)
	tokenized, _, _ = st.tokenize_sentences(TEST_SENTENCES)
	pred = model_intent(tokenized)
	pred = softmax(pred[0])
	# pred_feedback = model_feedback(tokenized)
	# pred_feedback = softmax(pred_feedback[0])
	# print(pred_feedback)
	out = {}
	output = {}
	# print(type(pred[0]))
	output["news"] = round(pred[0].astype(float),3)
	output["query"] = round(pred[1].astype(float),3)
	output["spam/junk"] = round(pred[2].astype(float),3)
	output["marketing"] = round(pred[3].astype(float),3)
	output["feedback/opinion"] = round(pred[4].astype(float),3)
	# output["feedback/opinion"]["score"]=round(pred[4].astype(float),3)
	# output["feedback/opinion"]["complaint"]= 
	final_output = {}
	final_output["intent"] = max(output, key=output.get)
	final_output["probabilities"] = output
	# out["intent"] = final_output

	#print(final_output)
	return (final_output)


# sent = input("Give sentence: ")

# test_emotion_texts("I am not happy.")
#print(test_sentiment_texts("I am not happy."))
# test_intent_texts("I am not happy.")

