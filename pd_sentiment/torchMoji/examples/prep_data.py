import pandas as pd
import pickle as pkl
from random import shuffle

def prep(df, class_dict):
    data = {}
    data['texts'] = df['texts'].tolist()
    import pdb
    pdb.set_trace()
    data['info'] = [{'label': l } for l in df['label'].map(class_dict)]
    indices = list(range(len(df)-1))
    shuffle(indices)
    data['train_ind'] = indices[:int(0.8*len(df))]
    data['val_ind'] = indices[int(0.8*len(df)):int(0.9*len(df))]
    data['test_ind'] = indices[int(0.9*len(df)):]
    with open('../data/emotion_data/raw.pickle', 'wb') as f:
        pkl.dump(data, f)


emotion_dict = {'happy': 0,
                "sad": 1,
                "bored":1,
                "fear":1,
                "excited": 2,
                "funny":2,
                "angry":3,
                "disgusted":3,
                "indifferent":4,
                "None":4
}

df = pd.read_csv('../data/emotion_data/data.csv')
prep(df, emotion_dict)
