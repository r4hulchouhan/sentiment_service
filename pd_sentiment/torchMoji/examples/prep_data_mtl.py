import pandas as pd
import pickle as pkl
from random import shuffle

def prep(df, sentiment_dict, emotion_dict, intent_dict):
    data = {}
    data['texts'] = df['texts'].tolist()
    import pdb
    pdb.set_trace()
    # data['info'] = [{'label': l } for l in df['label'].map(class_dict)]
    data['emotion'] = [l for df['emotion'].map(emotion_dict)]
    data['sentiment'] = [l for df['sentiment'].map(sentiment_dict)]
    data['intent'] = [l for df['intent'].map(lambda x: intent_dict[x] if x in intent_dict else 4)]
    df['info'] = zip(data['sentiment'], data['emotion'], data['intent'])
    indices = list(range(len(df)-1))
    shuffle(indices)
    data['train_ind'] = indices[:int(0.8*len(df))]
    data['val_ind'] = indices[int(0.8*len(df)):int(0.9*len(df))]
    data['test_ind'] = indices[int(0.9*len(df)):]
    with open('../data/mtl_data/raw.pickle', 'wb') as f:
        pkl.dump(data, f)


sentiment_dict = {'Negative':0,
                  'Neutral':1,
                  'Positive':2
                  }

intent_dict = {'news':0,
               'query':1,
               'spam/junk':2,
               'random':2,
               'marketing':3}


emotion_dict = {'happy': 0,
                "sad": 1,
                "bored":1,
                "fear":1,
                "excited": 2,
                "funny":2,
                "angry":3,
                "disgusted":3,
                "indifferent":4,
                "None":4
}

df = pd.read_csv('../data/emotion_data/data.csv')
prep(df, sentiment_dict, emotion_dict, intent_dict)
