# -*- coding: utf-8 -*-
""" Global variables.
"""
import tempfile
from os.path import abspath, dirname
import os
from google.cloud import storage
import configparser

config = configparser.ConfigParser()

config.read('path.ini')
model_path=config['MODEL']['PATH']





# The ordering of these special tokens matter
# blank tokens can be used for new purposes
# Tokenizer should be updated if special token prefix is changed
SPECIAL_PREFIX = 'CUSTOM_'
SPECIAL_TOKENS = ['CUSTOM_MASK',
                  'CUSTOM_UNKNOWN',
                  'CUSTOM_AT',
                  'CUSTOM_URL',
                  'CUSTOM_NUMBER',
                  'CUSTOM_BREAK']
SPECIAL_TOKENS.extend(['{}BLANK_{}'.format(SPECIAL_PREFIX, i) for i in range(6, 10)])

ROOT_PATH = dirname(dirname(abspath(__file__)))
if os.path.isfile(model_path + "vocabulary.json")==True:
        VOCAB_PATH =model_path + 'vocabulary.json'
else:
        client = storage.Client()
        bucket = client.get_bucket('apis_models')
        blob = bucket.blob('sentiment/vocabulary.json')
        blob.download_to_filename(model_path + 'vocabulary.json')
        VOCAB_PATH =model_path + 'vocabulary.json'

#VOCAB_PATH = '{}/model/vocabulary.json'.format(ROOT_PATH)
#PRETRAINED_PATH = '{}/model/pytorch_model.bin'.format(ROOT_PATH)
#print (PRETRAINED_PATH)
WEIGHTS_DIR = tempfile.mkdtemp()

NB_TOKENS = 50000
NB_EMOJI_CLASSES = 64
FINETUNING_METHODS = ['last', 'full', 'new', 'chain-thaw']
FINETUNING_METRICS = ['acc', 'weighted']
