#!/usr/bin/python3

__author__ = "Manish Kumar"

from functools import reduce
from raven     import Client

import traceback
import datetime
import logging
import pymongo
import redis
import json
import pika

# Importing Error Responses
from settings import error_500
from settings import error_api_key
from settings import error_premium_apis
from settings import error_invalid_input
from settings import error_basic_daily_quota
from settings import error_basic_monthly_quota
from settings import error_visual_daily_quota
from settings import error_visual_monthly_quota
from settings import error_excel_daily_quota
from settings import error_excel_monthly_quota

from settings import rabbit_mq_parameters
from settings import sentry_credentials
from settings import redis_parameters

def open_redis_connection():
	return redis.StrictRedis( host= redis_parameters, port= 6379, db= 0, password= "mongodude123" )



# Redis Connection Details
#red    = redis.StrictRedis( host= redis_parameters, port= 6379, db= 0, password= "mongodude123" )

# Sentry Credentials
client = Client( sentry_credentials )

# Rabbit MQ Connection
rmq_parameters                = pika.URLParameters( rabbit_mq_parameters )
rmq_parameters.socket_timeout = 300
rmq_connection                = pika.BlockingConnection( parameters= rmq_parameters )
rmq_channel                   = rmq_connection.channel()
rmq_channel.queue_declare( queue= "update_redis"   , durable= True )
rmq_channel.queue_declare( queue= "register_hit"   , durable= True )
rmq_channel.queue_declare( queue= "register_miss"  , durable= True )
rmq_channel.queue_declare( queue= "register_reject", durable= True )
rmq_channel.queue_declare( queue= "usage_alert"    , durable= True )

# Mongo DB Connection Details
MONGO_DB_SERVER_URL       = "mongodev.paralleldots.com"
DB_NAME                   = "api_server"

COLLECTION_HITS           = "hits"
COLLECTION_MISS           = "miss"
COLLECTION_REJECT         = "reject"

VISUAL_COLLECTION_HITS    = "visual_hits"
VISUAL_COLLECTION_MISS    = "visual_miss"
VISUAL_COLLECTION_REJECT  = "visual_reject"

EXCEL_COLLECTION_HITS     = "excel_hits"
EXCEL_COLLECTION_MISS     = "excel_miss"
EXCEL_COLLECTION_REJECT   = "excel_reject"

PREMIUM_COLLECTION_HITS   = "premium_hits"
PREMIUM_COLLECTION_MISS   = "premium_miss"
PREMIUM_COLLECTION_REJECT = "premium_reject"

def output_code_check( output ):
	if "code" not in output:
		output[ "code" ] = 200
	return output

def check_api_key_blocked(api_key):
	red = open_redis_connection()
	try:
		key_exists = red.get( 'blocked' )
		if key_exists:
			try:
				data = json.loads( key_exists.decode() )
				if api_key in data:
					return True
			except:
				logging.error( "Error <500>: %s"%traceback.format_exc() )
				client.captureMessage( "Error in `check_api_key_blocked`: %s"%traceback.format_exc() )
				return error_500
		else:
			return False
	except Exception as e:
		print(e)
	finally:
		del red

def check_api_key_rate_limit_restriction(api_key):
	red = open_redis_connection()
	try:
		key_exists = red.get( 'bypass' )
		if key_exists:
			try:
				data = json.loads( key_exists.decode() )
				if api_key in data:
					return True
			except:
				logging.error( "Error <500>: %s"%traceback.format_exc() )
				client.captureMessage( "Error in `check_api_key_rate_limit_restriction`: %s"%traceback.format_exc() )
				return error_500
		else:
			return False
	except Exception as e:
		print(e)
	finally:
		del red


def validate_and_execute( api_key, api_name, weights, function_call, *arguments ):
	red = open_redis_connection()
	try:
		if api_key in [ "", None, "None" ]:
			return error_api_key

		key_exists = red.get( api_key )

		if key_exists:
			try:
				data = json.loads( key_exists.decode() )

				if data[ "paying" ] == True:
					if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
					
						if "package_id" in data:

							if data[ "daily_quota" ] - weights <= 0:
								return error_basic_daily_quota

							if data[ "monthly_quota" ] - weights <= 0:
								return error_basic_monthly_quota

						output = function_call( arguments )
						if "Error" not in output:
							update_redis( "basic", api_key, api_name, weights )
							return output
						else:
							return output_code_check( output )
					else:
						return error_invalid_input

				elif data[ "daily_quota" ] - weights >= 0 and data[ "monthly_quota" ] - weights >= 0 :
					if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
						output = function_call( arguments )
						if "Error" not in output:
							update_redis( "basic", api_key, api_name, weights )

							post_usage_data = data#red.get( api_key ).decode()
							usage_Email( post_usage_data, "text", api_key )

							return output
						else:
							return output_code_check( output )
				else:
					remains_daily   = True if data[ "daily_quota"   ] - weights >= 0 else False
					remains_monthly = True if data[ "monthly_quota" ] - weights >= 0 else False
					if remains_daily == False:
						return error_basic_daily_quota
					if remains_monthly == False:
						return error_basic_monthly_quota
			except:
				logging.error( "Error <500>: %s"%traceback.format_exc() )
				client.captureMessage( "Error in `validate_and_execute`: %s"%traceback.format_exc() )
				return error_500
		else:
			return error_api_key
	except Exception as e:
		print(e)
	finally:
		del red




def visual_validate_and_execute( api_key, api_name, weights, function_call, *arguments ):
	red = open_redis_connection()
	
	try:
		if api_key in [ "", None, "None" ]:
			return error_api_key

		key_exists = red.get( api_key )

		if key_exists:
			try:
				data = json.loads( key_exists.decode() )

				if data[ "paying" ] == True:
					if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
						output = function_call( arguments )
						if "Error" not in output:
							update_redis( "visual", api_key, api_name, weights )
							return output
						else:
							return output_code_check( output )
					else:
						return error_invalid_input

				elif data[ "visual_daily_quota" ] - weights >= 0 and data[ "visual_monthly_quota" ] - weights >= 0 :
					if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
						output = function_call( arguments )
						if "Error" not in output:
							update_redis( "visual", api_key, api_name, weights )
							post_usage_data = data
							usage_Email( post_usage_data, "visual", api_key )
							return output
						else:
							return output_code_check( output )
				else:
					visual_remains_daily   = True if data[ "visual_daily_quota"   ] - weights >= 0 else False
					visual_remains_monthly = True if data[ "visual_monthly_quota" ] - weights >= 0 else False
					if visual_remains_daily == False:
						return error_visual_daily_quota
					if visual_remains_monthly == False:
						return error_visual_monthly_quota
			except:
				logging.error( "Error <500>: %s"%traceback.format_exc() )
				client.captureMessage( "Error in `visual_validate_and_execute`: %s"%traceback.format_exc() )
				return error_500
		else:
			return error_api_key
	except Exception as e:
		print(e)
	finally:
		del red

def excel_validate_and_execute( api_key, api_name, weights, function_call, *arguments ):
	red = open_redis_connection()
	try:
		if api_key in [ "", None, "None" ]:
			return error_api_key

		key_exists = red.get( api_key )

		if key_exists:
			try:
				data = json.loads( key_exists.decode() )

				if data[ "paying" ] == True and data['package_id'] != 1:
					if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
						output = function_call( arguments )
						if "Error" not in output:
							update_redis( "excel", api_key, api_name, weights )
							return output
						else:
							return output_code_check( output )
					else:
						return error_invalid_input

				elif data[ "excel_daily_quota" ] - weights >= 0 and data[ "excel_monthly_quota" ] - weights >= 0 :
					if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
						output = function_call( arguments )
						if "Error" not in output:
							update_redis( "excel", api_key, api_name, weights )
							post_usage_data = data
							usage_Email( post_usage_data, "text", api_key )

							return output
						else:
							return output_code_check( output )
				else:
					remains_d_excel = True if data[ "excel_daily_quota"   ] - weights >= 0 else False
					remains_m_excel = True if data[ "excel_monthly_quota" ] - weights >= 0 else False
					remains_daily   = True if data[ "daily_quota"         ] - weights >= 0 else False
					remains_monthly = True if data[ "monthly_quota"       ] - weights >= 0 else False

					if remains_d_excel == False:
						return error_excel_daily_quota
					if remains_m_excel == False:
						return error_excel_monthly_quota
					if remains_daily == False:
						return error_basic_daily_quota
					if remains_monthly == False:
						return error_basic_monthly_quota
			except:
				try:
					data = json.loads( key_exists.decode() )

					if data[ "paying" ] == True: #and data['package_id'] != 1:
						if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
							output = function_call( arguments )
							print("rery",output)
							if "Error" not in output:
								update_redis( "excel", api_key, api_name, weights )
								return output
							else:
								return output_code_check( output )
						else:
							return error_invalid_input

					elif data[ "excel_daily_quota" ] - weights >= 0 and data[ "excel_monthly_quota" ] - weights >= 0 :
						#print
						if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
							output = function_call( arguments )
							if "Error" not in output:
								update_redis( "excel", api_key, api_name, weights )
								post_usage_data = data
								usage_Email( post_usage_data, "text", api_key )

								return output
							else:
								return output_code_check( output )
					else:
						remains_d_excel = True if data[ "excel_daily_quota"   ] - weights >= 0 else False
						remains_m_excel = True if data[ "excel_monthly_quota" ] - weights >= 0 else False
						remains_daily   = True if data[ "daily_quota"         ] - weights >= 0 else False
						remains_monthly = True if data[ "monthly_quota"       ] - weights >= 0 else False

						if remains_d_excel == False:
							return error_excel_daily_quota
						if remains_m_excel == False:
							return error_excel_monthly_quota
						if remains_daily == False:
							return error_basic_daily_quota
						if remains_monthly == False:
							return error_basic_monthly_quota
				except:
					logging.error( "Error <500>: %s"%traceback.format_exc() )
					client.captureMessage( "Error in `excel_validate_and_execute`: %s"%traceback.format_exc() )
					return error_500
		else:
			return error_api_key
	except Exception as e:
		print(e)
	finally:
		del red

def premium_validate_and_execute( api_key, api_name, weights, function_call, *arguments ):
	red = open_redis_connection()
	try:
		if api_key in [ "", None, "None" ]:
			return error_api_key

		key_exists = red.get( api_key )

		if key_exists:
			try:
				data = json.loads( key_exists.decode() )

				if data[ "paying" ] == True:
					if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
						output = function_call( arguments )
						if "Error" not in output:
							update_redis( "premium", api_key, api_name, weights )
							return output
						else:
							return output_code_check( output )
					else:
						return error_invalid_input
				else:
					return error_premium_apis
			except:
				logging.error( "Error <500>: %s"%traceback.format_exc() )
				client.captureMessage( "Error in `premium_validate_and_execute`: %s"%traceback.format_exc() )
				return error_500
		else:
			return error_api_key

	except Exception as e:
		print(e)
	finally:
		del red


def keyless_validate_and_execute( function_call, *arguments ):
	if reduce( lambda y, z: y and z, map( lambda x: type( x ) == str and x not in [ "", None ], arguments ) ):	# Ensures input arguments are not None or Empty Strings
		output = function_call( arguments )
		if "Error" not in output:
			return output
		else:
			return output_code_check( output )
	else:
		return error_invalid_input

def quota_usage( redis_object ):
	if redis_object[ "paying" ] == False:
		return_object = { "daily_quota": redis_object[ "daily_quota" ], "monthly_quota": redis_object[ "monthly_quota" ], "visual_daily_quota": redis_object[ "visual_daily_quota" ], "visual_monthly_quota": redis_object[ "visual_monthly_quota" ], "excel_daily_quota": redis_object[ "excel_daily_quota" ], "excel_monthly_quota": redis_object[ "excel_monthly_quota" ] }
	else:
		return_object = { "Error": "Paying Users are not restricted by Usage Quotas." }
	return return_object

def hits_made( redis_object ):
	daily_billable_hits_breakdown   = redis_object.get( "daily_billable_hits"  , {} )
	monthly_billable_hits_breakdown = redis_object.get( "monthly_billable_hits", {} )
	daily_billable_hits             = sum( daily_billable_hits_breakdown.values()   )
	monthly_billable_hits           = sum( monthly_billable_hits_breakdown.values() )
	return_object = { "daily_billable_hits": daily_billable_hits, "monthly_billable_hits": monthly_billable_hits, "daily_billable_hits_breakdown": daily_billable_hits_breakdown, "monthly_billable_hits_breakdown": monthly_billable_hits_breakdown }
	return return_object

def user_usage( api_key ):
	red = open_redis_connection()
	try:
		key_exists = red.get( api_key )
		if key_exists:
			data = json.loads( key_exists.decode() )
			if data[ "paying" ] == False:
				return quota_usage( data )
			else:
				return hits_made( data )
		else:
			return error_api_key

	except Exception as e:
		print(e)
	finally:
		del red


def billable_hit_usage( api_key ):
	red = open_redis_connection()
	try:
		key_exists = red.get( api_key )
		if key_exists:
			data = json.loads( key_exists.decode() )
			return hits_made( data )
		else:
			return error_api_key
	except Exception as e:
		print(e)
	finally:
		del red

def Rabbit_MQ_Connection():
	global rmq_connection
	global rmq_channel
	count = 1

	while rmq_connection.is_open == False and count <= 3:
		logging.info( "Attempting to Reconnect to Rabbit MQ... Attempt #%d"%count )
		try:
			rmq_parameters                = pika.URLParameters( rabbit_mq_parameters )
			rmq_parameters.socket_timeout = 300
			rmq_connection                = pika.BlockingConnection( parameters= rmq_parameters )
			rmq_channel                   = rmq_connection.channel()
			rmq_channel.queue_declare( queue= "update_redis"   , durable= True )
			rmq_channel.queue_declare( queue= "register_hit"   , durable= True )
			rmq_channel.queue_declare( queue= "register_miss"  , durable= True )
			rmq_channel.queue_declare( queue= "register_reject", durable= True )
			rmq_channel.queue_declare( queue= "usage_alert"    , durable= True )
		except:
			logging.error( "Failed Attempt #%d"%count )
		count = count + 1

	if rmq_connection.is_open:
		logging.info( "Rabbit MQ Reconnected." )
		return True
	else:
		logging.info( "Rabbit MQ Failed to Reconnect." )
		client.captureMessage( "Rabbit MQ Failed to Reconnect." )
		return False

def update_redis( validate_and_execute_type, api_key, api_name, weights ):
	global rmq_connection
	global rmq_channel
	data                = { "validate_and_execute_type": validate_and_execute_type, "api_key": api_key, "api_name": api_name, "weights": weights }
	if rmq_connection.is_open:
		complete = False
		while complete == False:
			try:
				rmq_channel.basic_publish( exchange= "", routing_key= "update_redis", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
				complete = True
				return

			except Exception as e:
				Rabbit_MQ_Connection()

	else:
		success = Rabbit_MQ_Connection()

		if success:
			# if Reconnected Successfully, then Re-attempt
			try:
				rmq_channel.basic_publish( exchange= "", routing_key= "update_redis", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
				return

			except Exception as e:
				logging.error( e )
				client.captureMessage( "Error Dumping to `update_redis` Queue in Fallback post Reconnect: %s"%traceback.format_exc() )
				return

		else:
			# If Reconnection Fails then Fallback
			red = open_redis_connection()
			try:
				# Loading the User Table Data for the API Key
				user_table = json.loads( red.get( api_key ).decode() )

				# Updates the Daily Billable Hits & Monthly Billable Hits Objects in Redis
				if user_table.get( "daily_billable_hits"  , None ) == None:
					user_table[ "daily_billable_hits"   ] = { api_name  : weights }
				else:
					if api_name in user_table[ "daily_billable_hits" ]:
						user_table[ "daily_billable_hits"   ][ api_name ] = user_table[ "daily_billable_hits"   ][ api_name ] + weights
					else:
						user_table[ "daily_billable_hits"   ][ api_name ] = weights

				if user_table.get( "monthly_billable_hits", None ) == None:
					user_table[ "monthly_billable_hits" ] = {}
				else:
					if api_name in user_table[ "monthly_billable_hits" ]:
						user_table[ "monthly_billable_hits" ][ api_name ] = user_table[ "monthly_billable_hits" ][ api_name ] + weights
					else:
						user_table[ "monthly_billable_hits" ][ api_name ] = weights

				# Updates the Quota Values in Redis
				if validate_and_execute_type == "basic":
					if user_table[ "daily_quota" ] - weights >= 0 and user_table[ "monthly_quota" ] - weights >= 0 :
						user_table[ "daily_quota"   ] = user_table[ "daily_quota"   ] - weights
						user_table[ "monthly_quota" ] = user_table[ "monthly_quota" ] - weights
					else:
						user_table[ "daily_quota"   ] = 0 if user_table[ "daily_quota"   ] - weights < 0 else user_table[ "daily_quota"   ] - weights
						user_table[ "monthly_quota" ] = 0 if user_table[ "monthly_quota" ] - weights < 0 else user_table[ "monthly_quota" ] - weights

				elif validate_and_execute_type == "visual":
					if user_table[ "visual_daily_quota" ] - weights >= 0 and user_table[ "visual_monthly_quota" ] - weights >= 0 :
						user_table[ "visual_daily_quota"   ] = user_table[ "visual_daily_quota"   ] - weights
						user_table[ "visual_monthly_quota" ] = user_table[ "visual_monthly_quota" ] - weights
					else:
						user_table[ "visual_daily_quota"   ] = 0 if user_table[ "visual_daily_quota"   ] - weights < 0 else user_table[ "visual_daily_quota"   ] - weights
						user_table[ "visual_monthly_quota" ] = 0 if user_table[ "visual_monthly_quota" ] - weights < 0 else user_table[ "visual_monthly_quota" ] - weights

				elif validate_and_execute_type == "excel":
					if user_table[ "excel_daily_quota" ] - weights >= 0 and user_table[ "excel_monthly_quota" ] - weights >= 0 and user_table[ "daily_quota" ] - weights >= 0 and user_table[ "monthly_quota" ] - weights >= 0 :
						user_table[ "daily_quota"   ]       = user_table[ "daily_quota"         ] - weights
						user_table[ "monthly_quota" ]       = user_table[ "monthly_quota"       ] - weights
						user_table[ "excel_daily_quota" ]   = user_table[ "excel_daily_quota"   ] - weights
						user_table[ "excel_monthly_quota" ] = user_table[ "excel_monthly_quota" ] - weights
					else:
						user_table[ "daily_quota"   ]       = 0 if user_table[ "daily_quota"   ]       - weights < 0 else user_table[ "daily_quota"   ]       - weights
						user_table[ "monthly_quota" ]       = 0 if user_table[ "monthly_quota" ]       - weights < 0 else user_table[ "monthly_quota" ]       - weights
						user_table[ "excel_daily_quota" ]   = 0 if user_table[ "excel_daily_quota"   ] - weights < 0 else user_table[ "excel_daily_quota"   ] - weights
						user_table[ "excel_monthly_quota" ] = 0 if user_table[ "excel_monthly_quota" ] - weights < 0 else user_table[ "excel_monthly_quota" ] - weights

				# Setting the Updated Values
				red.set( api_key, json.dumps( user_table ) )
				logging.info( "Updated Key [ %s ] in Redis."%api_key )

			except:
				logging.error(         "Critical Failure in `update_redis` Fallback Code. %s"%traceback.format_exc() )
				client.captureMessage( "Critical Failure in `update_redis` Fallback Code. %s"%traceback.format_exc() )
			finally:
				del red

def register_hit( api_classification, real_ip, headers, api_type, api_key, weights, output, inputs ):
	global rmq_connection
	global rmq_channel
	data = { "api_classification": api_classification, "real_ip": real_ip, "headers": dict( headers ), "api_type": api_type, "api_key": api_key, "weights": weights, "output": output, "inputs": inputs, "timestamp": datetime.datetime.now().strftime( "%Y-%m-%d %H:%M:%S.%f" ) }

	if rmq_connection.is_open:
		complete = False
		while complete == False:
			try:
				rmq_channel.basic_publish( exchange= "", routing_key= "register_hit", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
				complete = True
				return

			except Exception as e:
				Rabbit_MQ_Connection()
	else:
		success = Rabbit_MQ_Connection()

		if success:
			# if Reconnected Successfully, then Re-attempt
			try:
				rmq_channel.basic_publish( exchange= "", routing_key= "register_hit", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
				return

			except Exception as e:
				logging.error( e )
				client.captureMessage( "Error Dumping to `register_hit` Queue in Fallback post Reconnect: %s"%traceback.format_exc() )
				return
		else:
			# If Reconnection Fails then Fallback
			red = open_redis_connection()
			try:
				conn_hits                 = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_HITS           ]
				visual_conn_hits          = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ VISUAL_COLLECTION_HITS    ]
				excel_conn_hits           = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ EXCEL_COLLECTION_HITS     ]
				premium_conn_hits         = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ PREMIUM_COLLECTION_HITS   ]
				registry_hits             = { "basic": conn_hits, "visual": visual_conn_hits, "excel": excel_conn_hits, "premium": premium_conn_hits }

				try:
					data[ "timestamp" ] = datetime.datetime.strptime( data[ "timestamp" ], "%Y-%m-%d %H:%M:%S.%f" )
					registry_hits[ api_classification ].insert( data )
					logging.info( "MongoDB ID: [ %s ]"%data[ "_id" ] )
					return

				except:
					logging.error( traceback.format_exc() )
					client.captureMessage( "Error in Executing of `register_hit` Queue: %s"%( traceback.format_exc() ) )
					return

			except:
				logging.error(         "Critical Failure in `register_hit` Fallback Code. %s"%traceback.format_exc() )
				client.captureMessage( "Critical Failure in `register_hit` Fallback Code. %s"%traceback.format_exc() )
			finally:
				del red

def register_miss( api_classification, real_ip, headers, api_type, api_key, output, inputs ):
	global rmq_connection
	global rmq_channel
	data = { "api_classification": api_classification, "real_ip": real_ip, "headers": dict( headers ), "api_type": api_type, "api_key": api_key, "output": output, "inputs": inputs, "timestamp": datetime.datetime.now().strftime( "%Y-%m-%d %H:%M:%S.%f" ) }
	if rmq_connection.is_open:
		complete = False
		while complete == False:
			try:
				rmq_channel.basic_publish( exchange= "", routing_key= "register_miss", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
				complete = True
				return

			except Exception as e:
				Rabbit_MQ_Connection()
	else:
		success = Rabbit_MQ_Connection()

		if success:
			# if Reconnected Successfully, then Re-attempt
			try:
				rmq_channel.basic_publish( exchange= "", routing_key= "register_miss", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
				return

			except Exception as e:
				logging.error( e )
				client.captureMessage( "Error Dumping to `register_miss` Queue in Fallback post Reconnect: %s"%traceback.format_exc() )
				return
		else:
			# If Reconnection Fails then Fallback
			red = open_redis_connection()
			try:
				conn_miss                 = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_MISS           ]
				visual_conn_miss          = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ VISUAL_COLLECTION_MISS    ]
				excel_conn_miss           = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ EXCEL_COLLECTION_MISS     ]
				premium_conn_miss         = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ PREMIUM_COLLECTION_MISS   ]
				registry_miss             = { "basic": conn_miss, "visual": visual_conn_miss, "excel": excel_conn_miss, "premium": premium_conn_miss }

				try:
					data[ "timestamp" ] = datetime.datetime.strptime( data[ "timestamp" ], "%Y-%m-%d %H:%M:%S.%f" )
					registry_miss[ api_classification ].insert( data )
					logging.info( "MongoDB ID: [ %s ]"%data[ "_id" ] )
					return

				except:
					logging.error( traceback.format_exc() )
					client.captureMessage( "Error in Executing of `register_miss` Queue: %s"%( traceback.format_exc() ) )
					return

			except:
				logging.error(         "Critical Failure in `register_miss` Fallback Code. %s"%traceback.format_exc() )
				client.captureMessage( "Critical Failure in `register_miss` Fallback Code. %s"%traceback.format_exc() )
			finally:
				del red

def register_reject( api_classification, real_ip, headers, api_type, api_key, output, inputs ):
	global rmq_connection
	global rmq_channel
	data = { "api_classification": api_classification, "real_ip": real_ip, "headers": dict( headers ), "api_type": api_type, "api_key": api_key, "output": output, "inputs": inputs, "timestamp": datetime.datetime.now().strftime( "%Y-%m-%d %H:%M:%S.%f" ) }
	if rmq_connection.is_open:
		complete = False
		while complete == False:
			try:
				rmq_channel.basic_publish( exchange= "", routing_key= "register_reject", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
				complete = True
				return

			except Exception as e:
				Rabbit_MQ_Connection()
	else:
		success = Rabbit_MQ_Connection()

		if success:
			# if Reconnected Successfully, then Re-attempt
			try:
				rmq_channel.basic_publish( exchange= "", routing_key= "register_reject", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
				return

			except Exception as e:
				logging.error( e )
				client.captureMessage( "Error Dumping to `register_reject` Queue in Fallback post Reconnect: %s"%traceback.format_exc() )
				return
		else:
			# If Reconnection Fails then Fallback
			try:
				conn_reject               = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ COLLECTION_REJECT         ]
				visual_conn_reject        = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ VISUAL_COLLECTION_REJECT  ]
				premium_conn_reject       = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ PREMIUM_COLLECTION_REJECT ]
				excel_conn_reject         = pymongo.MongoClient( MONGO_DB_SERVER_URL )[ DB_NAME ][ EXCEL_COLLECTION_REJECT   ]
				registry_reject           = { "basic": conn_reject, "visual": visual_conn_reject, "excel": excel_conn_reject, "premium": premium_conn_reject }

				try:
					data[ "timestamp" ] = datetime.datetime.strptime( data[ "timestamp" ], "%Y-%m-%d %H:%M:%S.%f" )
					registry_reject[ api_classification ].insert( data )
					logging.info( "MongoDB ID: [ %s ]"%data[ "_id" ] )
					return

				except:
					logging.error( traceback.format_exc() )
					client.captureMessage( "Error in Executing of `register_reject` Queue: %s"%( traceback.format_exc() ) )
					return

			except:
				logging.error(         "Critical Failure in `register_reject` Fallback Code. %s"%traceback.format_exc() )
				client.captureMessage( "Critical Failure in `register_reject` Fallback Code. %s"%traceback.format_exc() )

def register( api_classification, real_ip, headers, api_type, api_key, weights, output, **inputs ):
	if "Error" in output:
		if output[ "Error" ] == error_api_key:
			register_reject( api_classification, real_ip, headers, api_type, api_key, output, inputs )
		else:
			register_miss( api_classification, real_ip, headers, api_type, api_key, output, inputs )
	else:
		register_hit( api_classification, real_ip, headers, api_type, api_key, weights, output, inputs )

def usage_Email( data, api_name, api_key ):
	global rmq_connection
	global rmq_channel
	data                = json.dumps( data )
	data                = eval( data.replace( "true", "True" ).replace( "false", "False" ) )
	data[ "type"     ]  = api_name
	data[ "api_key"  ]  = api_key

	if rmq_connection.is_open:
		try:
			rmq_channel.basic_publish( exchange= "", routing_key= "usage_alert", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
			return

		except Exception as e:
			logging.error( e )
	else:
		Rabbit_MQ_Connection()
		try:
			rmq_channel.basic_publish( exchange= "", routing_key= "usage_alert", body= json.dumps( data ), properties= pika.BasicProperties( delivery_mode= 2 ) )
			return

		except Exception as e:
			logging.error( e )
			client.captureMessage( "Error Dumping to `usage_alert` Queue Fallback: %s"%traceback.format_exc() )
			return

def check_daily_quota(api_key):
	data = json.loads(red.get(api_key).decode())
	if data['daily_quota'] ==0:
		return True
	else:
		return False