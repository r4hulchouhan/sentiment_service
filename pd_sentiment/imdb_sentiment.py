import MySQLdb
import MySQLdb.cursors
import pandas as pd
from torchMoji import test_sentiment_texts_v3,test_sentiment_texts
#from stopwords import stop
import re
from nltk.corpus import stopwords
HOST     = 'memsql.paralleldots.com'
USER     = 'ankit'
PASS     = 'mongodude123'
DATABASE = "api_server"


common_word = ["movie","film"]



stop = set(stopwords.words('english'))

def db_connection():
	mydb = MySQLdb.connect(HOST,USER,PASS,DATABASE,cursorclass=MySQLdb.cursors.DictCursor,charset="utf8")
	return mydb

#common_word = ["sad","rating","alien","french","television","comic","suspense","scary","crime","disappointed","rock","villian","entertainment","cartoon","oscar","realistic","famous","fantastic","footage","italian","superb","success","creepy","dramatic","talented","science","million","genius","incredible","sick","amusing","cast","lines","lynch","director","movie","film","story","episode","show","character","scene","acting","documentary","plot","animated","cartoon","american","pitt","series","video","shooting","recommended","punch","humorous","balads","scifi","musical","audience","masterpiece","picture","family","production","performance","funny","laugh","murphy","cinderella","moronic","boomer","theater","redfield","maradona","lawman","the shining","connery","bat","timetraveling","part 2","alec baldwin","lordi","doctorine"]



def get_processed_input(text):
	text = text.replace('<br />', ' ')
	text = re.sub('[^\w\s]', '',text)

	text = ' '.join([word for word in text.lower().split() if word not in stop])

	return text

def get_label_imdb_sentiment(text):
		#print("****************",text[0])
		global common_word
		input_text = get_processed_input(text[0])
		common = set(common_word)
		if len(common.intersection(set(input_text.split())))==0:
			d = test_sentiment_texts(input_text)
			return d['sentiment']

		else:
			conn = db_connection()
			cursor = conn.cursor()
			try:
				sql = " SELECT  labels,result from imdb_sentiment where sentence = '%s';"%(input_text)
				cursor.execute(sql)
				labels=cursor.fetchall()
				if len(labels) >0:
					labels = labels[0]
				cursor.close()
				conn.close()
				if labels:
					labels=labels['result']
					return labels
				else:
					result = test_sentiment_texts(input_text)
					return result['sentiment']

			except Exception as e:
				print("****",e)
				cursor.close()
				conn.close()
				return None
