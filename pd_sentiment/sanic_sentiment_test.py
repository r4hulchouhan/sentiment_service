from sanic_sentiment_api import app
import json
text    = "Prime Minister Narendra Modi tweeted a link to the speech Human Resource Development Minister Smriti Irani made in the Lok Sabha during the debate on the ongoing JNU row and the suicide of Dalit scholar Rohith Vemula at the Hyderabad Central University."

batch_text      = json.dumps(["Apple was founded by Steve Jobs.","Apple Inc. is an American multinational technology company headquartered in Cupertino, California"])

api_key = "sU6x7TOjTcIUPdSY7tUbbB3v38ViKeQw2RrPkiXjEgw"

JSON_DATA= {"sentiment":{"negative":0.185,"neutral":0.549,"positive":0.266}}

BATCH_JSON_DATA= {"sentiment":[{"negative":0.156,"neutral":0.139,"positive":0.705},{"negative":0.039,"neutral":0.179,"positive":0.782}]}

def test_sentiment():
        request, response = app.test_client.post('/v4/sentiment',data={ "text": text, "api_key": api_key })
        assert type(response.json) is dict
        assert response.status == 200
        assert response.json == JSON_DATA
def test_sentiment_batch():
        request, response = app.test_client.post('/v4/sentiment_batch',data={ "text": batch_text, "api_key": api_key })
        assert type(response.json) is dict
        assert response.status == 200
        assert response.json == BATCH_JSON_DATA
test_sentiment()
test_sentiment_batch()

