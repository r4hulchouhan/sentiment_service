# import boto3
import uuid
import requests
# translate = boto3.client(service_name='translate', region_name='us-west-2', use_ssl=True)

# def aws_translate(text,lang_code):
#     text = text[0:1500]

#     result = translate.translate_text(Text=text,SourceLanguageCode=lang_code, TargetLanguageCode="en")

#     print('TranslatedText: ' + result.get('TranslatedText'))

#     return result.get('TranslatedText')
    # print('SourceLanguageCode: ' + result.get('SourceLanguageCode'))
    # print('TargetLanguageCode: ' + result.get('TargetLanguageCode'))



def azure_translate(text_input, language_code):
    text_input = text_input[0:1500]
    base_url = 'https://api.cognitive.microsofttranslator.com'
    path = '/translate?api-version=3.0'
    params = '&to=en'
    constructed_url = base_url + path + params

    headers = {
        'Ocp-Apim-Subscription-Key': '3b8c7138ffbd4eec8c34c1ad1b30c891',
        'Ocp-Apim-Subscription-Region': 'eastasia',
        'Content-type': 'application/json',
        'X-ClientTraceId': str(uuid.uuid4())
    }
    body = [{
        'text' : text_input
    }]
    response = requests.post(constructed_url, headers=headers, json=body)
    data = response.json()
    if len(data)!=0:
        data = data[0]
        return data['translations'][0].get('text','') if len(data['translations'])!=0 else ''