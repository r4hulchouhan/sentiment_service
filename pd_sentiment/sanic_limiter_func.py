import sys
#sys.path.insert(0,'/datadrive/manish/pd_security')
import security
import json
import requests
def check_paid_user(api_key,lang_code="en"):
	if api_key is not None:
		red = security.open_redis_connection()
		try:
			api_hits_check = red.get(api_key)
			if api_hits_check is not None:
				user_type = json.loads(api_hits_check)
				if user_type['paying']:
					try:
						if user_type['package_id'] == 1:
							if lang_code.lower() != "en":
								return {"code":403,"package_name":"Free"}
							url = "http://apis.paralleldots.com/v3/sanic/package/free"

						elif user_type['package_id'] == 2:
							if lang_code.lower() != "en":
								return {"code":403,"package_name":"Starter"}
							url = "http://apis.paralleldots.com/v3/sanic/package/starter"

						elif user_type['package_id'] == 7:
							print(user_type['package_id'])
							if lang_code.lower() != "en":
								return {"code":403,"package_name":"Starter"}
							url = "http://apis.paralleldots.com/v3/sanic/package/starter"

						elif user_type['package_id'] == 5:
							if lang_code.lower() != "en":
								return {"code":403,"package_name":"Basic"}
							url = "http://apis.paralleldots.com/v3/sanic/package/basic"

						elif user_type['package_id'] == 9:
							url = "http://apis.paralleldots.com/v3/sanic/package/standard"

						elif user_type['package_id'] == 3:
							url = "http://apis.paralleldots.com/v3/sanic/package/standard"
						elif user_type['package_id'] == 4:
							url = "http://apis.paralleldots.com/v3/sanic/package/business"
						elif user_type['package_id'] == 10:
							url = "http://apis.paralleldots.com/v3/sanic/package/business_299"
						else:
							url = "http://apis.paralleldots.com/v3/sanic/package/advanced"
					except:
						url = "http://apis.paralleldots.com/v3/sanic/package/business"
				else:
					if lang_code.lower() != "en":
						return {"code":403,"package_name":"Free"}
					url = "http://apis.paralleldots.com/v3/sanic/package/free"
			else:
				if lang_code.lower() not in  ["nl","en","de","fr","it","es"]:
					return {"code":403,"package_name":"Free"}
				else:
					return {"code":400}
				url = "http://apis.paralleldots.com/v3/sanic/package/free"
			payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n"+api_key+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
			headers = {
				'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
				'Cache-Control': "no-cache"
				}
			resp = requests.post(url, data=payload, headers=headers).json()
			return resp
		except Exception as e:
			print(e)
		finally:
			del red
	else:
		return {"code":400}
